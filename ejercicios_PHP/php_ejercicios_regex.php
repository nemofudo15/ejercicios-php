<?php 
    function escape ($regular_exp, $delimiter){
        $regular_exp = str_split($regular_exp);
        $new_exp = "";
        $del_touch = "";
        foreach($regular_exp as $char){    
            switch ($char) {
                case '.':
                case '\ ':     
                case '+':
                case '*':
                case '?':
                case '[':
                case '^':
                case ']':
                case '$':                           
                case '(':    
                case ')':
                case '{':
                case '}':
                case ' =':
                case '!':
                case '<':
                case '>':
                case '|':
                case ':':
                case '-':
                case '#':
                case $delimiter:    
                    $del_touch.= ' \ '.$char;
                    break;
                
                default:
                    $del_touch.=$char;
                    break;
            } 
        }
        return $del_touch;
    }
/*----------------Ejercicio 1
Realizar una expresión regular que detecte emails correctos.
Para realizar esto considere que tuviera por lo menos un caracter: númerico, minuscula, mayuscula y como opción extra agregar un caracter especial "."ó "_"*/
    $check_1 = "1nemofudO@gmail.com";
    $result_1 = preg_match('/^(([._]*[A-z]*[._]*[0-9]*[._]*|[._]*[0-9]*[._]*[A-z]*[._]*)(([._]*[a-z]+[._]*[A-Z]+[._]*[0-9]+[._]*)|([._]*[A-Z]+[._]*[a-z]+[._]*[0-9]+[._]*)|([._]*[A-Z]+[._]*[0-9]+[._]*[a-z]+[._]*)|([._]*[0-9]+[._]*[A-Z]+[._]*[a-z]+[._]*)|([._]*[0-9]+[._]*[a-z]+[._]*[A-Z]+[._]*)|([._]*[a-z]+[._]*[0-9]+[._]*[A-Z]+[._]*))([._]*[A-z]*[._]*[0-9]*[._]*|[._]*[0-9]*[._]*[A-z]*[._]*))@[A-z]+[.]com([.][a-z]{2,})*/ ',$check_1);
    echo $result_1;
    echo "<br>";

/*--------------Ejercicio 2
Realizar una expresion regular que detecte Curps Correctos */    
    $check_2 = "ABCD123456EFGHIJ78";
    $result_2 = preg_match('/^[A-Z][AEIOU][A-Z][A-Z][0-9]{2}(((1(0|2){1}|0(1|3|5|7|8){1})([0][1-9]|[1-2][0-9]|[3][0-1]))|(1(1){1}|0(4|6|9))([0][1-9]|[1-2][0-9]|[3][0])|02([0][1-9]|[1-2][0-8]))(H|M)[A-Z]{2}[^AEIOUa-z]{3}[0-9]{2}/ ',$check_2);
    echo $result_2;
    echo "<br>";
/*-------------Ejercicio 3
Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras. */    
    $check_3 ="aaaaaaaaaaccccccBxxxxxxxx ";
    $result_3 = preg_match('/^[A-z]{50,}/ ',$check_3);
    echo $result_3;  
    echo "<br>";
    /*Ejercicio 4*/
    echo escape('$40. for a g3/400' , '/'); 
    echo "<br>";
    /*Ejercicio 5
    La composición de los números decimales es la siguiente, por una parte están conformados por un elemento entero y otra decimal, las cuales se separan una de la otra por símbolos como la coma o el punto. */
    $check_5 = "15.369";
    
    $result_5 = preg_match('/^[1-9][0-9]*[.][0-9]+/ ',$check_5);
    echo $result_5;
?>