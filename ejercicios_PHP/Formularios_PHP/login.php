<?php
/*if (session_status() == 2) {
  session_destroy(); 
}*/
session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style  type="text/css">
         @import url("https://fonts.googleapis.com/css2?family=Quicksand&display=swap");
         body {
            background: url(https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.hdwallpaper.nu%2Fwp-content%2Fuploads%2F2017%2F04%2Fpurple-8.png&f=1&nofb=1);
            background-attachment: fixed;
            background-size: cover;
            font-family: "Quicksand", sans-serif;
            display: grid;
            grid-template: 2fr 50% 1fr/2fr 3fr;
            box-sizing: content-box;
            color: white;    
        }
        
        #maint {
            grid-column-start: 1;
            grid-column-end:3;
            grid-row: 1;
            background: #323E42;
            font-size:2.5em;   
            
        }
        #phpim {
          grid-column: 2;
          grid-row: 2;
          max-width: 100%;
          max-height: 60%;
          min-height: 15%;
          min-width: 15%;
          opacity: 50%;
         
}

        form {
            grid-column: 2;
            grid-row : 2;
            margin-top: 50px;
            margin-bottom: 50px;
            display: grid;
            background : #323E42;
            border-radius: 25%;
        }

        #numC{
          display: grid;
          grid-column: 1;
          grid-row: 1;
          margin: 3% 25%;
          font-size: 1.5em;
        }
        
        #passW{
          display: grid;
          grid-row: 5;
          grid-column: 1;
          margin: 3% 25%;
          font-size:1.5em;
        }

        #bottom {
          grid-row: 6;
          margin: 5% 25%;
          background: #7954A1;
          text-align:center;
          text-decoration:none;
          font-size: 1.5em;
          color: #EFDCF9;
        }

        #logo_image{
          display: grid;
          grid-column: 1;
          grid-row: 2;
          grid-template: 15% 1fr 15%/15% 1fr 15%;
        }
        
        #footer{
          grid-rows : 3;
          grid-column-start: 1;
          grid-column-end: 3;
          text-align: center;
          font-size: .9em;
          

        }
    </style>
    <title>Login</title>
</head>
<body>
   
      <h1 id="maint" >Login_Prueba</h1>
      <div id = "logo_image">
        <img
          id="phpim"
          src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fdab1nmslvvntp.cloudfront.net%2Fwp-content%2Fuploads%2F2016%2F04%2F1459870313PHP-logo.svg.png&f=1&nofb=1"
          alt="php_imagen"
        />
      </div>
     
    
    <form action="info.php" method="post">
      <!--Inserción de valores de elementos-->
      <!--La etiqueta requiered fue puesta para forzar llenar todos los campos que  solcitan -->
      <div id = "numC">
        <label id="label-num_cuenta" for="text">Número de Cuenta: </label>
        <input
          id="input-num_cuenta"
          type="text"
          name="num_cuenta"
          placeholder="Número de cuenta"
          required
        />
      </div>
     
      <div id = "passW">
        <label for="input-password">Contraseña: </label>
        <input id="input-password" type="password" name="password" placeholder="Ingrese contraseña" required />
      </div>
      
      <br />
      <!---Botones-->
      <input id="bottom" type="submit" value="Ingresar" />
    </form>

    <div id="footer">
    Derechos reservados: 
     Lorem ipsum dolor sit amet consectetur adipisicing elit.
      <br>
        2019-2020
    </div>
    
    
  </body>
</html>

<?php 
/*Usuario por default */
 $_SESSION["alumno_1"] = 
 [   "num_de_cuenta" => 1, 
     "nombre" => "Admin",
     "primer_apellido" => "General",
     "segundo_apellido"=> "",
     "contra" => "adminpass123.",
     "g" => 'O',
     "date" => date('1990-25-01')
      ];

      
  $_SESSION["formulario"] = 0;
  unset($_SESSION["num_cuenta"]);
  $_SESSION["info"] = 0;    
  
  /*Variable que me servira para identificar si se encontro al usuario registrado o no  */
?>