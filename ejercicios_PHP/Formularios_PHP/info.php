<?php
    session_start();
    /*Función que me sirvira para redireccionar a login.php*/ 
    $user_found = false;
    function escape(){
        
        header('Location: login.php');
        exit;
    }
    if ((isset($_POST["num_cuenta"]))){
        $_SESSION["num_cuenta_aux"] = $_POST["num_cuenta"]; 
    }
    $_SESSION["num_cuenta"] = ((isset($_POST["num_cuenta"]))) ? $_POST["num_cuenta"] : $_SESSION["num_cuenta_aux"] ; 
    
    /* Válido que se haya ingresado al login.php primero */   
    if(((!(isset($_POST["num_cuenta"])))) &&  ($_SESSION["formulario"] == 0) ){
        unset($_SESSION["num_cuenta"]);
        escape();
    }else {
        $_SESSION["formulario"] = 1;
    }
    if(!empty($_SESSION)){
        $_SESSION["num_ingreso"] = (sizeof($_POST) == 2) ? $_SESSION["num_cuenta"] : $_SESSION["num_ingreso"] ;
        $n_cuenta = $_SESSION["num_ingreso"];
        $_SESSION["alumno_"."$n_cuenta"]["data_s"] = (isset($_SESSION["alumno_"."$n_cuenta"]["data_s"]))? $_SESSION["alumno_"."$n_cuenta"]["data_s"] :  array($_SESSION["alumno_"."$n_cuenta"]);     
        
        if(sizeof($_POST) == 2 ){
            foreach($_SESSION as $key => $value){
                $cuenta = "alumno_".$n_cuenta;
                if($key == $cuenta){
                    $user_found = true;
                    /*Si la contraseña ingresada no coincide  con la original lo saca de info.php*/
                    if ($_POST["password"] != $_SESSION[$key]["contra"]){
                        escape();
                    }
                    break; 
                    
                }
                
            }
            /*Si no encuentra al usuario lo saca */
            if(!$user_found) {
                escape();
            }
        }
    }
?>    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        header{
            background:#323E42; 
            display: grid; 
            grid-template: 100%/1fr 1fr 1fr  1fr 1fr;
            font-size: 2.5em;
        }
        a{
            text-decoration:none;
            color: white;
            margin:5px;
            box-sizing: content-box;
            /*padding: 25px,0;*/

            
        }
        table{
            width: 30%; 
            font-size: 1.5em;
            background: #EFDCF9;
            
            
        }

        th{
            background: #323E42;
            color: white;
            opacity: 50%;

        }
        #perfil{
            opacity:100%; 
            grid-column:1; 
            grid-row:2;
            border-radius:50%
        }

    </style>
    <title>Info.php</title>
</head>
<body>
    <header>
        <a href="#" style="grid-column:1; ">Home</a>
        <a href="./formulario.php" style ="grid-column:3; "> Registrar Sesión</a>
        <a href="./login.php" style="grid-column:5; ">Cerrar sesión</a>
    </header>
        
    <main>
        <div id =user_a >
            <h1>Usuario Autenticado</h1>
            <table id ="general_info">
                <tr>
                    
                            
                   
                    <th>
                    <img src="https://i.pravatar.cc/128 " id="perfil" alt="avatar  del usuario">       
                    <?php
                        $num = $_SESSION["num_ingreso"];
                        $full_name = $_SESSION["alumno_".$num]["nombre"]." ".$_SESSION["alumno_".$num]["primer_apellido"]." ".$_SESSION["alumno_".$num]["segundo_apellido"]; ?>
                        <?php echo $full_name  ?> 
                    </th>
                </tr>

            <tr>
                <td>
                    Información
                </td>
            </tr>
            <tr>
                <td>
                    Numero de cuenta: <?php  print $num?>
                </td>
            </tr>
            <tr>
                <td>
                    Fecha de Nacimiento: <?php print $_SESSION["alumno_".$num]["date"] ?>  
                </td>
            </tr>    
            
        </table>
        </div>
        <div id=save_d>
            <h1>Datos guardados:</h1>
            <table>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Fecha de Nacimiento</th>
                </tr>

                <?php
                    if (isset($_POST)){
                        if ( sizeof($_POST) > 2 ){
                            /* Primero llenar la sección con el alumno inscrito*/
                            $alumno_n = $_POST["num_de_cuenta"]; //numero de cuenta del usuario
                            
                            $_SESSION["alumno_".$alumno_n] = $_POST;
                            
                            
                           
                            
                            if (!(isset($_SESSION["arr"]))){
                                $_SESSION["arr"] = array() ;/*Variable auxliar que me resguardara los datos guardados del usuario x*/
                            }
                            $_SESSION["arr"] = $_SESSION["alumno_".$n_cuenta]["data_s"];
                            // print_r($_SESSION["arr"]);
                            
                            $_SESSION["arr"]["alumno_".$alumno_n] = $_SESSION["alumno_".$alumno_n];
                            $_SESSION["alumno_".$n_cuenta]["data_s"] = $_SESSION["arr"];
                           
                           
                            $_SESSION["formulario"] = 2;
                            
                        }
                    }
                    
                    $data_register = $_SESSION["alumno_".$n_cuenta]["data_s"];

                    foreach ($data_register as $key => $value) {
                        $name = $data_register[$key]["nombre"]." ".$data_register[$key]["primer_apellido"]." ".$data_register[$key]["segundo_apellido"];
                            print '<tr>';
                            print '<td>'.$data_register[$key]["num_de_cuenta"].'</td>';
                            print '<td>'.$name.'</td>';
                            print '<td>'.$data_register[$key]["date"].'</td>';
                            print '</tr>'; 
                    }
                    
                    
                ?>
            </table>
        </div>
    </main>
    <?php
        if (isset ($_POST["num_cuenta"])){
            unset($_POST["num_cuenta"]);
        }
        
     ?> 
</body>
</html>
