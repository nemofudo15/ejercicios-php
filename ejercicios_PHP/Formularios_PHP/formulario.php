
<?php 
    function escape(){
        header('Location: login.php');
        exit;
    }
    session_start();
    if (!(isset($_SESSION["num_cuenta"])) &&  ($_SESSION["formulario"] == 0)){
        escape();
    }else{
        $_SESSION["formulario"] = 1;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        header{
            background:#323E42; 
            display: grid; 
            grid-template: 100%/1fr 1fr 1fr  1fr 1fr;
            font-size: 2.5em;
        }
        a{
            text-decoration:none;
            color: white;
            margin:5px;
            box-sizing: content-box;
            
        }
        /* 9 filas  */
        form{
          display:grid;
          grid-template: 1fr 1fr 1fr 1fr 6fr 1fr 1fr 2fr  /100%;
          ; 
          background-color: #7954A1;
          

        }
        .form_s{
          display:grid;
          margin-bottom:10px;
          margin-top:10px;
          font-size: 1.2em;
          color:white;
        }
        #r_box{
          display:grid;
          grid-template: 1fr 1fr 1fr 1fr/ 1fr 3fr 1fr; 
          font-size: 2em;
          
        }
        #bottom{
          
          grid-template: 100% /1fr 1fr 2fr 1fr 1fr  2fr  1fr;
        }
        .bottoms{
          background-color: #C55FFC;
          font-size:1.5em;
          color:white;
        }

        #h, #o, #m{
          grid-column: 2;
          text-align:center;
        }
        #h{
          grid-row:2;
        }
        #m{
          grid-row:3;
        }
        #o{
          grid-row:4;
        }

        #enter{
          grid-row: 1;
          grid-column: 3;
          
        }
        #clear{
          grid-row:1;
          grid-column:6;
        }
    </style>
    <title>Formulario</title>
</head>
<body>
    <header>
        <a href="./info.php" style="grid-column:1; ">Home</a>
        <a href="#" style ="grid-column:3; "> Registrar Sesión</a>
        <a href="./login.php" style="grid-column:5; ">Cerrar sesión</a>
    </header>
    <main>
        
        <form action="info.php" method="post">
        <div id= "num_c" class= "form_s">
        <label id="label-numero_nuevo" for="text">Número de Cuenta: </label>
        <input
          id="input-num_cuenta_nuevo"
          type="number"
          name="num_de_cuenta"
          placeholder="Número de cuenta"
          min= "2" 
          required
        />
        </div>
         
        <div id= "name" class="form_s">
          <label id="label-nombre_nuevo" for="text">Nombre: </label>
          <input
            id="input-nombre_nuevo"
            type="text"
            name="nombre"
            placeholder="Nombre"
          />
        </div>
        
        <div id="f_name" class="form_s">
          <label id="label-primer_a_n" for="text">Primer Apellido: </label>
          <input
            id="input-primer_a_n"
            type="text"
            name="primer_apellido"
            placeholder="primer apellido"
          />
        </div>
       
        <div id="l_name" class ="form_s" >
          <label id="label-segundo_a_n" for="text">Segundo Apellido: </label>
          <input
            id="input-segundo_a_n"
            type="text"
            name="segundo_apellido"
            placeholder="segundo apellido"
          />
        </div>
        <div id="r_box" class="form_s">
          Seleccione género por favor
          </p>
          <div id="h">
            <input type="radio" name="g"  value='H' />Hombre
          </div>
          
          <br>
          <div id="m">
            <input type="radio" name="g"  value='M' />Mujer
          </div>
          <br>
          <div id="o">
          <input type="radio" name="g"  value='O'  checked />Otro
          </div>
          
        </div>
            
        
        
        <div id="date" class= "form_s">
          <label id="label-fecha_n_nuevo" for="text">Fecha de Nacimiento: </label>
          <input
            id="input-fecha_n_nuevo"
            type="date"
            name="date"
            placeholder="dd/mm/aaaa";
          />
        </div>
        <div id="password" class ="form_s" >
          <label id="label-contrasena_nuevo" for="text">Contraseña: </label>
          <input
            id="input-contrasena_nuevo"
            type="password"
            name="contra"
            placeholder="Contrasena"
            required
          />
        </div>
        
        <div id= "bottom" class="form_s" >
        <input  id ="enter" type="submit" class= "bottoms" value="Ingresar" />
        <input  id="clear" type="reset" class="bottoms" value="Limpiar" />
        </div>
        
    </form>

    </main>
</body>
</html>