<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Piramide y Rombo</title>
</head>
    
<body>
    <?php 
        $ROWS = 30;
        echo "<h1> PIRAMIDE </h1>";
        for($i = 1; $i <= $ROWS; $i++){
            echo ' <div style = 
            "display: flex;
            justify-content: center;
            align-items: center; 
            margin-top = 0px;
            margin-bottom = 0px;">';
            for($j = 0; $j < $i; $j++){
                echo "*";
            }
            echo "</div>";
            /*
            */
        }
        echo "<h1> ROMBO </h1>";
        echo "<br />";
        for ($i = 1; $i< $ROWS*2; $i++){
            $sup_bound = 0;
            echo ' <div style = 
            "display: flex;
            justify-content: center;
            align-items: center; 
            margin-top = 0px;
            margin-bottom = 0px;">';
            if ($i < $ROWS){
               $sup_bound = $i % 31;
            }else {
                $sup_bound = ($ROWS - ( $i % $ROWS)); 
            }
            for($j = 0; $j < $sup_bound; $j++){
                echo "*";
            }
            echo "</div>";
        }
        
    ?>
</body>
</html>
